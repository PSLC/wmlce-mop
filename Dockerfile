# ##### Dockerfile for Watson Machine Learning Community Edition 1.7.0 #####
# sudo docker build -t registry.gitlab.com/pslc/docker/wmlce:1.7.0 .
# sudo docker push registry.gitlab.com/pslc/docker/wmlce:1.7.0

FROM ibmcom/powerai:1.7.0-base-ubuntu18.04-py37

# Default UID for user 'pwrai'
USER 2051

RUN sudo apt-get update && \
    sudo apt-get install -y \
 	build-essential \
 	cmake \
 	htop \
 	git \
 	zip \
 	unzip \
 	imagemagick \
 	screen

# Downgrade conda to 4.7.12 and pin it (because 4.8.x leads to package conflicts)
RUN /opt/anaconda/bin/conda install -y conda=4.7.12 && \
	echo "conda 4.7.12" >> /opt/anaconda/conda-meta/pinned

# Remove old WMLCE config (channel and 'wmlce' environment)
RUN /opt/anaconda/bin/conda config --remove channels https://public.dhe.ibm.com/ibmdl/export/pub/software/server/ibm-ai/conda/ && \
	/opt/anaconda/bin/conda env remove -n wmlce -y

# Add new OpenCE config (channel and 'opence' environment)
RUN /opt/anaconda/bin/conda config --prepend channels https://opence.mit.edu && \
	/opt/anaconda/bin/conda create -y -n opence python=3 pytorch tensorflow


# Install Jupyterlab in 'opence' conda environment
RUN /opt/anaconda/bin/conda run -n opence /opt/anaconda/bin/conda install -y \
	nodejs \
	jupyterlab

# Install the 'jupyterlab-nvdashboard' extension
RUN /opt/anaconda/bin/conda run -n opence /opt/anaconda/bin/conda install -c conda-forge -y jupyterlab-nvdashboard --no-channel-priority && \
    /opt/anaconda/bin/conda run -n opence /opt/anaconda/envs/opence/bin/jupyter-labextension install jupyterlab-nvdashboard

# Install Theia
COPY theia-package.json /tmp/theia-install/package.json
RUN cd /tmp/theia-install && \
	sudo chown -R 2051:2051 /tmp/theia-install && \
	/opt/anaconda/bin/conda create -y -n theia python=2 nodejs && \
	/opt/anaconda/bin/conda run -n theia /opt/anaconda/envs/theia/bin/npm install -g yarn && \
	/opt/anaconda/bin/conda run -n theia /opt/anaconda/envs/theia/bin/yarn && \
	/opt/anaconda/bin/conda run -n theia /opt/anaconda/envs/theia/bin/yarn theia build

# Install flake8 Python linter (needed by VSCode Python plugin)
RUN /opt/anaconda/bin/conda run -n opence /opt/anaconda/bin/conda install -y flake8 --no-channel-priority

# Install NGINX Proxy
RUN sudo apt-get install -y \
   nginx \
   apache2-utils

# Install htpasswd (required by set_password.py)
RUN /opt/anaconda/bin/conda run -n opence /opt/anaconda/envs/opence/bin/pip install htpasswd

# Clean conda cache
RUN /opt/anaconda/bin/conda clean --all -y

# Clean bashrc (remove license accept script as WMLCE is no longer installed, and activate the right 'opence' env)
RUN sed -i "s/^conda activate base$//g; s/^conda activate wmlce$/conda activate opence/g; s/^\/var\/local\/powerai\/license_accept.sh$//g" /home/pwrai/.bashrc

COPY start.sh /start.sh
COPY tini-ppc64le /tini
COPY set_password.py /usr/local/bin/set_password.py

WORKDIR /wmlce/data

RUN sudo chmod +x /usr/local/bin/set_password.py && \
	sudo chsh -s /bin/bash pwrai

ENV SHELL /bin/bash
 
#VOLUME /wmlce/data

EXPOSE 8888 8080 3000 22

CMD ["/tini", "/start.sh"]
