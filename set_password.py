#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json, os, sys, htpasswd, stat
from getpass import getpass
from notebook.auth import passwd

# 0. Get password ##############################################################
if len(sys.argv) > 1:
    new_passwd = sys.argv[1]
else:
    passwd1 = getpass("Enter new password: ")
    passwd2 = getpass("Confirm new password: ")
    if passwd1 != passwd2:
        raise Exception("Passwords differ.")
    new_passwd = passwd1

# 1. Set JupyterLab password ###################################################
jupyter_config = "/wmlce/data/.jupyter/jupyter_notebook_config.json"

if not os.path.isfile(jupyter_config):
    with open(jupyter_config, "w") as f:
        json.dump({"NotebookApp": {"password": passwd(new_passwd)}}, f, indent=2)

else:
    with open(jupyter_config, "r") as f:
        config = json.load(f)

    if not "NotebookApp" in config:
        config["NotebookApp"] = {}
    config["NotebookApp"]["password"] = passwd(new_passwd)

    with open(jupyter_config, "w") as f:
        json.dump(config, f, indent=2)

# 2. Set htpasswd ##############################################################
htpasswd_config = "/wmlce/data/.htpasswd"

# create empty htpasswd if it doesn't exist
if not os.path.isfile(htpasswd_config):
    os.mknod(htpasswd_config)

with htpasswd.Basic(htpasswd_config, mode="md5") as userdb:
    try:
        userdb.change_password("pwrai", new_passwd)
    except htpasswd.basic.UserNotExists:
        userdb.add("pwrai", new_passwd)

os.chmod(htpasswd_config, stat.S_IREAD | stat.S_IWRITE | stat.S_IRGRP | stat.S_IROTH)
