#!/bin/bash

echo 'export PATH=$PATH:/usr/local/nvidia/bin' >> /home/pwrai/.profile
echo 'export LICENSE=yes' >> /home/pwrai/.profile

#sudo chsh -s /bin/bash pwrai

sudo ldconfig

if [ -f /wmlce/data/powerai.init ]; then
    source /wmlce/data/powerai.init
else
    if [ -f /mnt/default/powerai.init.default ]; then
        cp -p /mnt/default/powerai.init.default /wmlce/data/powerai.init
        source /wmlce/data/powerai.init
    else
        echo "Can't find /mnt/default/powerai.init.default"
    fi
fi

# if [ ! -f /wmlce/data/.jupyter/jupyter_notebook_config.json ]; then
#     mkdir -p /wmlce/data/.jupyter
#     if [ -f /mnt/default/jupyter_notebook_config.json.default ]; then
#         cp -p /mnt/default/jupyter_notebook_config.json.default /wmlce/data/.jupyter/jupyter_notebook_config.json
#     else
#         echo "Can't find /mnt/default/jupyter_notebook_config.json.default"
#     fi
# fi

if [ ! -f /wmlce/data/.jupyter/lab/user-settings/@jupyterlab/docmanager-extension/plugin.jupyterlab-settings ] ; then
    mkdir -p /wmlce/data/.jupyter/lab/user-settings/@jupyterlab/docmanager-extension
    if [ -f /mnt/default/docmanager-extension.default ]; then
        cp /mnt/default/docmanager-extension.default /wmlce/data/.jupyter/lab/user-settings/@jupyterlab/docmanager-extension/plugin.jupyterlab-settings
    else
        echo "Can't find /mnt/default/docmanager-extension.default"
    fi
fi

# if [ ! -f /wmlce/data/.htpasswd ] ; then
#     if [ -f /mnt/default/htpasswd.default ]; then
#         cp /mnt/default/htpasswd.default /wmlce/data/.htpasswd
#     else
#         echo "Can't find /mnt/default/htpasswd.default"
#     fi
# fi

if [ ! -f /wmlce/data/.htpasswd ] ; then
    if [ ! -f /wmlce/data/.jupyter/jupyter_notebook_config.json ]; then
        echo "/opt/anaconda/envs/opence/bin/python /usr/local/bin/set_password.py $DEFAULT_PASSWORD"
        /opt/anaconda/envs/opence/bin/python /usr/local/bin/set_password.py $DEFAULT_PASSWORD
    else
        echo "Password not set because /wmlce/data/.jupyter/jupyter_notebook_config.json already exist"
    fi
else
    echo "Password not set because /wmlce/data/.htpasswd already exist"
fi

mkdir -p /wmlce/data/.theia
sudo ln -s /wmlce/data/.theia /home/pwrai/.theia

if [ ! -f /wmlce/data/.theia/recentworkspace.json ] ; then
    if [ -f /mnt/default/recentworkspace.json.default ]; then
        cp /mnt/default/recentworkspace.json.default /wmlce/data/.theia/recentworkspace.json
    else
        echo "Can't find /mnt/default/recentworkspace.json.default"
    fi
fi

if [ ! -f /wmlce/data/.theia/settings.json ] ; then
    if [ -f /mnt/default/settings.json.default ]; then
        cp /mnt/default/settings.json.default /wmlce/data/.theia/settings.json
    else
        echo "Can't find /mnt/default/settings.json.default"
    fi
fi


sudo ln -s /wmlce/ /powerai

cd /tmp/theia-install/
/opt/anaconda/bin/conda run -n theia /opt/anaconda/envs/theia/bin/yarn theia start --hostname 127.0.0.1 > /tmp/theia-install/run.log 2>&1 &

sudo service nginx start

# JUPYTER_CONFIG_DIR=/wmlce/data/.jupyter/
/opt/anaconda/envs/opence/bin/jupyter lab --ip=$(hostname) --no-browser  --notebook-dir=/wmlce/data

